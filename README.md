# Crypto Information

A description of Codes and Cyphers.

Illustrated with [Ruby Code](https://www.ruby-lang.org/en/)

Cryptanalysis example of attacking cyphers.

# Elementary Cyphers
## substitution  Cyphers
* Ceasar
* Atbash

## Transposition Cyphers
* Rail fence
* Twisted Path

## Substitution Cyphers
* Symbol 
* Pigpen

# Cryptanalysis

* Brute Force
* Frequency Counting
* N gram Counting

# References
## Cryptocraghy
* Codes, Ciphers and Secret Writing - Martin Gardner - [On Amazon](https://amzn.to/2uJuRKO)

##Cryptanalysis:


### older
* Elementary Cryptanalysis: a Mathemathical Approach - Abraham Sinkov. - Out of print.
* Cryptanalysis: a Study of ciphers and their solutions. - Helen Fouche Gaines. - Out of print.
